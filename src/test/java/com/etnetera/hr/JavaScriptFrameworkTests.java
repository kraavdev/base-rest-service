package com.etnetera.hr;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.ReleaseInfo;
import com.fasterxml.jackson.databind.JsonNode;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;


/**
 * Class used for Spring Boot/MVC based tests.
 *
 * @author Etnetera
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class JavaScriptFrameworkTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldSucceedOnSaveEntityAndContainPrimaryKeys() {
        postEntityAndVerify("test_name", 1, 1);
    }

    private JavaScriptFramework postEntityAndVerify(final String name,
                                                    final int version,
                                                    final int hypeLevel) {
        final JavaScriptFramework framework = new JavaScriptFramework();
        framework.setName(name);
        framework.setReleases(new LinkedHashSet<>());
        framework.getReleases().add(new ReleaseInfo(version, new Date(), hypeLevel, framework));

        final ResponseEntity<JavaScriptFramework> response = restTemplate.postForEntity(
            "/frameworks",
            framework,
            JavaScriptFramework.class);
        final JavaScriptFramework responseEntity = response.getBody();

        Assert.assertEquals(200, response.getStatusCode().value());
        Assert.assertNotNull(responseEntity);
        Assert.assertNotNull(responseEntity.getId());
        Assert.assertEquals(1, responseEntity.getReleases().size());
        Assert.assertEquals(framework.getName(), responseEntity.getName());
        final ReleaseInfo release = responseEntity.getReleases().iterator().next();
        Assert.assertNotNull(release.getId());
        Assert.assertEquals(version, release.getVersion());
        Assert.assertEquals(hypeLevel, release.getHypeLevel());
        return responseEntity;
    }

    @Test
    public void shouldSuccedOnUpdatingEntity() {
        final JavaScriptFramework responseEntity = postEntityAndVerify("test_name2", 1, 1);

        responseEntity.setName("changed_name");
        final ReleaseInfo releaseInfo = responseEntity.getReleases().iterator().next();
        releaseInfo.setDeprecationDate(new Date());
        releaseInfo.setHypeLevel(2);
        releaseInfo.setVersion(2);

        final HttpEntity<JavaScriptFramework> en = new HttpEntity<>(responseEntity);
        final ResponseEntity<JavaScriptFramework> updateResponse = restTemplate.exchange(
            "/frameworks",
            HttpMethod.PUT,
            en,
            JavaScriptFramework.class);
        final JavaScriptFramework updateResponseEntity = updateResponse.getBody();

        Assert.assertEquals(200, updateResponse.getStatusCode().value());
        Assert.assertNotNull(updateResponseEntity);
        Assert.assertNotNull(updateResponseEntity.getId());
        Assert.assertEquals(1, updateResponseEntity.getReleases().size());
        Assert.assertEquals("changed_name", updateResponseEntity.getName());
        final ReleaseInfo updatedRelease = updateResponseEntity.getReleases().iterator().next();
        Assert.assertNotNull(updatedRelease.getId());
        Assert.assertEquals(2, updatedRelease.getVersion());
        Assert.assertEquals(2, updatedRelease.getHypeLevel());

    }

    @Test
    public void shouldAddNewRelease() {
        final JavaScriptFramework responseEntity = postEntityAndVerify("test_name3", 1, 1);
        responseEntity.setName("changed_name2");
        final ReleaseInfo releaseInfo = responseEntity.getReleases().iterator().next();
        releaseInfo.setDeprecationDate(new Date());
        releaseInfo.setHypeLevel(2);
        releaseInfo.setVersion(2);
        releaseInfo.setFrameworkId(responseEntity);
        responseEntity.getReleases().add(new ReleaseInfo(3, new Date(), 3, responseEntity));

        final Set<ReleaseInfo> prevReleases = responseEntity.getReleases();

        final HttpEntity<JavaScriptFramework> en = new HttpEntity<>(responseEntity);
        final ResponseEntity<JavaScriptFramework> updateResponse = restTemplate.exchange(
            "/frameworks",
            HttpMethod.PUT,
            en,
            JavaScriptFramework.class);
        final JavaScriptFramework updateResponseEntity = updateResponse.getBody();
        final Set<ReleaseInfo> updatedReleases = updateResponseEntity.getReleases();

        Assert.assertEquals(200, updateResponse.getStatusCode().value());
        Assert.assertNotNull(updateResponseEntity);
        Assert.assertNotNull(updateResponseEntity.getId());
        Assert.assertEquals(2, updateResponseEntity.getReleases().size());
        Assert.assertEquals("changed_name2", updateResponseEntity.getName());


        final TreeSet<ReleaseInfo> previousReleasesSorted =
            new TreeSet<>(Comparator.comparing(ReleaseInfo::getVersion));
        previousReleasesSorted.addAll(prevReleases);
        final TreeSet<ReleaseInfo> currentReleasesSorted = new TreeSet<>(Comparator.comparing(ReleaseInfo::getVersion));
        currentReleasesSorted.addAll(updatedReleases);
        Assert.assertEquals(previousReleasesSorted.size(), currentReleasesSorted.size());
    }

    @Test
    public void shouldReturn409InCaseOfExistingItem() {
        final JavaScriptFramework framework = new JavaScriptFramework();
        framework.setName("test_name3");
        framework.setReleases(new LinkedHashSet<>());
        framework.getReleases().add(new ReleaseInfo(1, new Date(), 1, framework));

        final ResponseEntity<JavaScriptFramework> response = restTemplate.postForEntity(
            "/frameworks",
            framework,
            JavaScriptFramework.class);

        final JavaScriptFramework responseEntity = response.getBody();
        final ResponseEntity<JsonNode> theSameEntityResponse = restTemplate.postForEntity(
            "/frameworks",
            responseEntity,
            JsonNode.class);
        Assert.assertNotNull("Null response", theSameEntityResponse.getBody());
        Assert.assertEquals(409, theSameEntityResponse.getStatusCode().value());
        Assert.assertEquals("Data integrity violation", theSameEntityResponse.getBody().path("message").asText(""));
    }

    @Test
    public void shouldSucceedInUpdatingExistingItems() {
        final JavaScriptFramework framework = new JavaScriptFramework();
        framework.setName("test_nameToBeUpdated");
        framework.setReleases(new LinkedHashSet<>());
        framework.getReleases().add(new ReleaseInfo(1, new Date(), 1, framework));

        final ResponseEntity<JavaScriptFramework> response = restTemplate.postForEntity(
            "/frameworks",
            framework,
            JavaScriptFramework.class);

        final JavaScriptFramework responseEntity = response.getBody();
        response.getBody().getReleases().iterator().next().setVersion(2);
        final HttpEntity<JavaScriptFramework> en = new HttpEntity<>(responseEntity);
        final ResponseEntity<JavaScriptFramework> updateResponse = restTemplate.exchange(
            "/frameworks",
            HttpMethod.PUT,
            en,
            JavaScriptFramework.class);
        Assert.assertEquals(200, updateResponse.getStatusCode().value());
        Assert.assertEquals(2, updateResponse.getBody().getReleases().iterator().next().getVersion());
    }

    @Test
    public void shouldAddNewItem() {
        final JavaScriptFramework framework = new JavaScriptFramework();
        framework.setName("test_name4");
        framework.setReleases(new LinkedHashSet<>());
        framework.getReleases().add(new ReleaseInfo(1, new Date(), 1, framework));

        final ResponseEntity<JavaScriptFramework> response = restTemplate.postForEntity(
            "/frameworks",
            framework,
            JavaScriptFramework.class);

        final JavaScriptFramework responseEntity = response.getBody();
        responseEntity.getReleases().add(new ReleaseInfo(2, new Date(), 2, framework));
        final HttpEntity<JavaScriptFramework> en = new HttpEntity<>(responseEntity);
        final ResponseEntity<JavaScriptFramework> updateResponse = restTemplate.exchange(
            "/frameworks",
            HttpMethod.PUT,
            en,
            JavaScriptFramework.class);
        Assert.assertEquals(200, updateResponse.getStatusCode().value());
        Assert.assertEquals(2, updateResponse.getBody().getReleases().size());

    }

    @Test
    public void shouldDeleteSingleVersion() {
        final JavaScriptFramework insertedFramework = postEntityAndVerify("test_name5", 1, 1);
        final int insertedVersion = insertedFramework.getReleases().iterator().next().getVersion();
        restTemplate.delete("/frameworks/{id}/{version}", insertedFramework.getId(), insertedVersion);

        final ResponseEntity<JavaScriptFramework> emptyEntity = restTemplate.getForEntity(
            "/frameworks/{name}",
            JavaScriptFramework.class,
            insertedFramework.getName());

        Assert.assertEquals(200, emptyEntity.getStatusCode().value());
        Assert.assertEquals(0, emptyEntity.getBody().getReleases().size());
    }

    @Test
    public void shouldDeleteWholeFramework() {
        final JavaScriptFramework insertedFramework = postEntityAndVerify("test_name6", 1, 1);

        restTemplate.delete("/frameworks/{id}", insertedFramework.getId());
        final ResponseEntity<JsonNode> emptyEntity = restTemplate.getForEntity(
            "/frameworks/{name}",
            JsonNode.class,
            insertedFramework.getName());
        Assert.assertEquals(404, emptyEntity.getStatusCode().value());
        Assert.assertNotNull("Null response", emptyEntity.getBody());
        Assert.assertEquals("Framework not found", emptyEntity.getBody().path("message").asText(""));
    }

}
