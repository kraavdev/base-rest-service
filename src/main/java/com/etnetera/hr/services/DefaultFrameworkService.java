package com.etnetera.hr.services;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.ReleaseInfo;
import com.etnetera.hr.repository.JavaScriptFrameworkRepository;
import com.etnetera.hr.repository.ReleaseFrameworkRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class DefaultFrameworkService implements FrameworkService {

    private final JavaScriptFrameworkRepository repository;
    private final ReleaseFrameworkRepository releaseRepository;

    @Autowired
    public DefaultFrameworkService(JavaScriptFrameworkRepository repository,
                                   final ReleaseFrameworkRepository releaseRepository) {
        this.repository = repository;
        this.releaseRepository = releaseRepository;
    }

    @Override
    public JavaScriptFramework saveEntity(final JavaScriptFramework en) {
        en.setId(null);
        en.getReleases().forEach(rl -> {
            rl.setFrameworkId(null);
            rl.setId(null);
        });
        return repository.save(en);

    }

    @Override
    public ReleaseInfo saveEntity(final ReleaseInfo en) {
        return releaseRepository.save(en);
    }

    @Override
    public JavaScriptFramework updateEntity(final JavaScriptFramework javaScriptFramework) {
        javaScriptFramework.getReleases().forEach(rl -> rl.setFrameworkId(javaScriptFramework));
        final Optional<JavaScriptFramework> entity = repository.findById(javaScriptFramework.getId());
        if (entity.isPresent()) {
            JavaScriptFramework en = entity.get();
            final Set<Long> existingIds = javaScriptFramework.getReleases()
                .stream()
                .filter(r -> r.getId() != null)
                .map(ReleaseInfo::getId).collect(Collectors.toSet());
            final Set<ReleaseInfo> currentReleases = en.getReleases();
            currentReleases.removeIf(e -> existingIds.contains(e.getId()));
            currentReleases.addAll(javaScriptFramework.getReleases());
            en.setName(javaScriptFramework.getName());
            return repository.save(en);
        }

        return null;
    }

    @Override
    public void deleteRelease(final Long id,
                              final Integer versionId) {
        final Optional<JavaScriptFramework> framework = repository.findById(id);
        if (framework.isPresent()) {
            final Set<ReleaseInfo> releases = framework.get().getReleases();
            releases.remove(new ReleaseInfo(versionId, framework.get()));
            repository.save(framework.get());
        }

    }

    @Override
    public void deleteFramework(final Long id) {
        repository.deleteById(id);
    }

    @Override
    public Iterable<JavaScriptFramework> allFrameworks() {
        return repository.findAll();
    }

    @Override
    public JavaScriptFramework findByName(final String name) {
        return repository.findByName(name);
    }

}
