package com.etnetera.hr.services;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.data.ReleaseInfo;

public interface FrameworkService {

    JavaScriptFramework saveEntity(JavaScriptFramework en);

    ReleaseInfo saveEntity(ReleaseInfo en);

    JavaScriptFramework updateEntity(JavaScriptFramework en);

    void deleteRelease(Long id, Integer version);

    void deleteFramework(Long id);

    Iterable<JavaScriptFramework> allFrameworks();

    JavaScriptFramework findByName(String name);
}
