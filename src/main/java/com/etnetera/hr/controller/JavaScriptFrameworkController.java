package com.etnetera.hr.controller;

import com.etnetera.hr.data.JavaScriptFramework;
import com.etnetera.hr.exceptions.EntityNotFoundException;
import com.etnetera.hr.services.FrameworkService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Simple REST controller for accessing application logic.
 *
 * @author Etnetera
 */
@RestController
public class JavaScriptFrameworkController {

    private final FrameworkService frameworkService;

    @Autowired
    public JavaScriptFrameworkController(final FrameworkService frameworkService) {
        this.frameworkService = frameworkService;
    }

    @GetMapping("/frameworks")
    public Iterable<JavaScriptFramework> frameworks() {
        return frameworkService.allFrameworks();
    }

    @GetMapping("/frameworks/{name}")
    public JavaScriptFramework framework(@PathVariable String name) {
        final JavaScriptFramework entity = frameworkService.findByName(name);
        if (entity == null) {
            throw new EntityNotFoundException();
        }
        return entity;
    }

    @ResponseStatus(value = HttpStatus.CONFLICT,
                    reason = "Data integrity violation")
    @ExceptionHandler(DataIntegrityViolationException.class)
    public void conflict() {

    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND,
                    reason = "Framework not found")
    @ExceptionHandler(EntityNotFoundException.class)
    public void notFound() {
    }

    @PostMapping("/frameworks")
    public JavaScriptFramework save(@RequestBody @Valid JavaScriptFramework javaScriptFramework) {
        return frameworkService.saveEntity(javaScriptFramework);
    }


    @PutMapping("/frameworks")
    public JavaScriptFramework update(@RequestBody @Valid JavaScriptFramework javaScriptFramework) {
        return frameworkService.updateEntity(javaScriptFramework);
    }

    @DeleteMapping("/frameworks/{id}/{versionId}")
    public void deleteRelease(@PathVariable @NotNull Long id,
                              @PathVariable @NotNull Integer versionId) {
        frameworkService.deleteRelease(id, versionId);
    }

    @DeleteMapping("/frameworks/{id}")
    public void deleteFramework(@PathVariable @NotNull Long id) {
        frameworkService.deleteFramework(id);
    }
}
