package com.etnetera.hr.data;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "releases", uniqueConstraints =  @UniqueConstraint(columnNames={"version", "framework_id"}))
public class ReleaseInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotNull
    private int version;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deprecationDate;
    @NotNull
    private int hypeLevel;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="framework_id")
    @JsonBackReference
    private JavaScriptFramework frameworkId;

    public ReleaseInfo(@NotNull final int version,
                       final JavaScriptFramework frameworkId) {
        this.version = version;
        this.frameworkId = frameworkId;
    }

    public ReleaseInfo(@NotNull final int version,
                       final Date deprecationDate,
                       @NotNull final int hypeLevel,
                       final JavaScriptFramework frameworkId) {
        this.version = version;
        this.deprecationDate = deprecationDate;
        this.hypeLevel = hypeLevel;
        this.frameworkId = frameworkId;
    }

    public ReleaseInfo() {
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public Date getDeprecationDate() {
        return deprecationDate;
    }

    public void setDeprecationDate(final Date deprecationDate) {
        this.deprecationDate = deprecationDate;
    }

    public int getHypeLevel() {
        return hypeLevel;
    }

    public void setHypeLevel(final int hypeLevel) {
        this.hypeLevel = hypeLevel;
    }

    public JavaScriptFramework getFrameworkId() {
        return frameworkId;
    }

    public void setFrameworkId(final JavaScriptFramework frameworkId) {
        this.frameworkId = frameworkId;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }


    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ReleaseInfo that = (ReleaseInfo) o;
        return version == that.version &&
            Objects.equals(frameworkId, that.frameworkId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(version, frameworkId);
    }
}
