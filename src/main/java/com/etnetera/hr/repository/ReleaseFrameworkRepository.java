package com.etnetera.hr.repository;

import com.etnetera.hr.data.ReleaseInfo;

import org.springframework.data.repository.CrudRepository;

/**
 * Spring data repository interface used for accessing the data in database.
 *
 * @author Etnetera
 */
public interface ReleaseFrameworkRepository extends CrudRepository<ReleaseInfo, Long> {

}
