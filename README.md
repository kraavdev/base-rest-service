Run application as:
````
./gradlew bootRun
````
Swagger documentation for REST API can be found here after launching the application:
````
http://localhost:8080/swagger-ui.html
````